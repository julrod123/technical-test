plugins{
    java
    id("net.ltgt.apt-idea")
    jacoco
}

dependencies {

    compileOnly("org.immutables:value:2.7.4")
    annotationProcessor("org.immutables:value:2.7.4")
    compile("io.vavr:vavr:0.10.0")

    testCompile("junit", "junit", rootProject.ext.get("junitVersion") as String)
}

jacoco {
    toolVersion = rootProject.ext.get("jacocoToolVersion") as String
}
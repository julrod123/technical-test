package com.example.employee.infrastructure.cache;

import com.example.employee.domain.entities.Contract;
import com.example.employee.domain.entities.Employee;
import com.example.employee.domain.entities.ImmutableContract;
import com.example.employee.domain.entities.ImmutableEmployee;
import com.example.employee.domain.repositories.EmployeeRepository;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Option;

import java.util.HashMap;
import java.util.concurrent.Executor;

public class EmployeeCache implements EmployeeRepository {

  private HashMap<Integer, Employee> employeeCache = new HashMap<Integer, Employee>();
  private final Executor executor;

  public EmployeeCache(Executor executor, List<Employee> employees) {
    this.executor = executor;
    employees.map(employee -> employeeCache.put(employee.id(), employee));
  }

  @Override
  public Future<Employee> saveEmployee(Employee employee) {
    employeeCache.put(employee.id(), employee);
    return Future.of(executor, () -> {
      return employee;
    });
  }

  @Override
  public Future<Option<Employee>> getEmployee(int id) {
    return Future.of(executor, () -> {
      return Option.of(employeeCache.get(id));
    });
  }
}

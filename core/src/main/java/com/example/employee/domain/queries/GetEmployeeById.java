package com.example.employee.domain.queries;

import com.example.employee.domain.Environment;
import com.example.employee.domain.entities.DomainError;
import com.example.employee.domain.entities.Employee;
import com.example.employee.domain.entities.EmployeeErrorFactory;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;

public class GetEmployeeById implements Query<Employee> {

  private int id;

  public GetEmployeeById(int id) {
    this.id = id;
  }

  @Override
  public Future<Either<List<DomainError>, Employee>> response(Environment env) {
    return env.employeeRepository.getEmployee(id).map(r -> r.toEither(List.of(EmployeeErrorFactory.employeeNotExist(id))));
  }
}

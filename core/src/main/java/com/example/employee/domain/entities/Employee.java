package com.example.employee.domain.entities;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.immutables.value.Value;

import static io.vavr.control.Validation.invalid;

@Value.Immutable
public abstract class Employee {

  public abstract int id();

  public abstract String name();

  public abstract Contract contract();

  public static Validation<List<DomainError>, Employee> from(int id, String name, String contractType, int roleId, String roleName, String roleDescription, double hourlySalary, double monthlySalary) {

    final Validation<DomainError, Integer> idValidated = Option.of(id)
        .filter(n -> n > 0)
        .fold(() -> invalid(EmployeeErrorFactory.idIsEmpty()), Validation::valid);

    final Validation<DomainError, String> nameValidated = Option.of(name)
        .filter(str -> !str.isEmpty())
        .fold(() -> invalid(EmployeeErrorFactory.nameIsEmpty()), Validation::valid);

    final Validation<DomainError, Contract> validatedContract = Contract.from(contractType, roleId, roleName, roleDescription, monthlySalary, hourlySalary);

    return Validation.combine(idValidated, nameValidated, validatedContract)
        .ap((i, n, ct) -> ImmutableEmployee.builder().id(i).name(n).contract(ct).build())
        .mapError(Seq::toList)
        .map(u -> u);
  }
}

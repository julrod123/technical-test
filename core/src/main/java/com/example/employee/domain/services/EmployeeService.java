package com.example.employee.domain.services;

import com.example.employee.domain.entities.DomainError;
import com.example.employee.domain.entities.Employee;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;

public interface EmployeeService {

  Future<Either<List<DomainError>, Employee>> getEmployeeById(int id);
}

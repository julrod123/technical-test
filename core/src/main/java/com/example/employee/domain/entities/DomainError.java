package com.example.employee.domain.entities;

import org.immutables.value.Value;

@Value.Immutable
public interface DomainError {
  String code();

  String message();
}

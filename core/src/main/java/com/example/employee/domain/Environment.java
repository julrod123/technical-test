package com.example.employee.domain;

import com.example.employee.domain.repositories.EmployeeRepository;

public class Environment {

  public final EmployeeRepository employeeRepository;

  public Environment(EmployeeRepository usersRepo) {
    this.employeeRepository = usersRepo;
  }
}

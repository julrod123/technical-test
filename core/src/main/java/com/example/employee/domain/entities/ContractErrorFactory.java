package com.example.employee.domain.entities;

public class ContractErrorFactory {

  public final static DomainError incorrectContract() {
    return ImmutableDomainError.builder()
        .code("3")
        .message("Incorrect contract type")
        .build();
  }

  public final static DomainError roleIdIsEmpty() {
    return ImmutableDomainError.builder()
        .code("4")
        .message("role id can not be empty")
        .build();
  }

  public final static DomainError roleNameIsEmpty() {
    return ImmutableDomainError.builder()
        .code("5")
        .message("role name can not be empty")
        .build();
  }

  public final static DomainError salaryLessOrEqualThanZero() {
    return ImmutableDomainError.builder()
        .code("8")
        .message("Salary must be greater than zero")
        .build();
  }
}

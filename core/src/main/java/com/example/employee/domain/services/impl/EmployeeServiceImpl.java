package com.example.employee.domain.services.impl;

import com.example.employee.domain.Environment;
import com.example.employee.domain.entities.DomainError;
import com.example.employee.domain.entities.Employee;
import com.example.employee.domain.queries.GetEmployeeById;
import com.example.employee.domain.services.EmployeeService;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;

public class EmployeeServiceImpl implements EmployeeService {

  private final Environment environment;

  public EmployeeServiceImpl(Environment env) {
    this.environment = env;
  }

  @Override
  public Future<Either<List<DomainError>, Employee>> getEmployeeById(int id) {
    return new GetEmployeeById(id).response(environment);
  }
}

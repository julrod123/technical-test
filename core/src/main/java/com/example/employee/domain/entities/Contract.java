package com.example.employee.domain.entities;

import io.vavr.collection.Traversable;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.immutables.value.Value;

import static io.vavr.control.Validation.invalid;
import static io.vavr.control.Validation.valid;

abstract class ContractType {

  public static Validation<DomainError, ContractType> apply(String type) {
    if (!type.isEmpty() && type.contentEquals("HourlySalaryEmployee")) {
      return valid(new HourlyContract());
    } else if (!type.isEmpty() && type.contentEquals("MonthlySalaryEmployee")) {
      return valid(new MonthlyContract());
    } else {
      return invalid(ContractErrorFactory.incorrectContract());
    }
  }

  public abstract Boolean isMonthlyContract();

  public abstract String toString();
}

class MonthlyContract extends ContractType {
  @Override
  public Boolean isMonthlyContract() {
    return true;
  }

  @Override
  public String toString() {
    return "MonthlySalaryEmployee";
  }
}

class HourlyContract extends ContractType {
  @Override
  public Boolean isMonthlyContract() {
    return false;
  }

  @Override
  public String toString() {
    return "HourlySalaryEmployee";
  }
}

@Value.Immutable
public abstract class Contract {

  public abstract String type();

  public abstract int roleId();

  public abstract String roleName();

  public abstract String roleDescription();

  public abstract double salary();

  public static Validation<DomainError, Contract> from(String type, int roleId, String roleName, String roleDescription, double monthlySalary, double hourlySalary) {

    final Validation<DomainError, ContractType> typeValidated = ContractType.apply(type);

    final Validation<DomainError, Integer> rolIdValidated = Option.of(roleId)
        .filter(n -> n > 0)
        .fold(() -> invalid(ContractErrorFactory.roleIdIsEmpty()), Validation::valid);

    final Validation<DomainError, String> roleNameValidated = Option.of(roleName)
        .filter(str -> !str.isEmpty())
        .fold(() -> invalid(ContractErrorFactory.roleNameIsEmpty()), Validation::valid);

    final Validation<DomainError, Double> monthlySalaryValidated = Option.of(monthlySalary)
        .filter(n -> n > 0.0)
        .fold(() -> invalid(ContractErrorFactory.salaryLessOrEqualThanZero()), Validation::valid);

    final Validation<DomainError, Double> hourlySalaryValidated = Option.of(hourlySalary)
        .filter(n -> n > 0.0)
        .fold(() -> invalid(ContractErrorFactory.salaryLessOrEqualThanZero()), Validation::valid);

    return Validation.combine(typeValidated, rolIdValidated, roleNameValidated, monthlySalaryValidated, hourlySalaryValidated)
        .ap((t, ri, rn, ms, hs) -> {
          if (t.isMonthlyContract())
            return ImmutableContract.builder().type(t.toString()).roleId(ri).roleName(rn).roleDescription(roleDescription).salary(ms).build();
          else
            return ImmutableContract.builder().type(t.toString()).roleId(ri).roleName(rn).roleDescription(roleDescription).salary(hs).build();
        })
        .mapError(Traversable::head)
        .map(u -> u);
  }

  public double calculateYearlySalary() {
    if (this.type().contentEquals("HourlySalaryEmployee")) {
      return 120 * this.salary() * 12;
    } else {
      return this.salary() * 12;
    }
  }
}

package com.example.employee.domain.repositories;

import com.example.employee.domain.entities.Employee;
import io.vavr.concurrent.Future;
import io.vavr.control.Option;

public interface EmployeeRepository {

  Future<Employee> saveEmployee(Employee employee);

  Future<Option<Employee>> getEmployee(int id);
}

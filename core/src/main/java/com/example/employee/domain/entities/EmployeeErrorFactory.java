package com.example.employee.domain.entities;

public class EmployeeErrorFactory {

  private EmployeeErrorFactory() {
    // to prevent instantiation
  }

  public final static DomainError idIsEmpty() {
    return ImmutableDomainError.builder()
        .code("1")
        .message("Id can not be empty")
        .build();
  }

  public final static DomainError nameIsEmpty() {
    return ImmutableDomainError.builder()
        .code("2")
        .message("name can not be empty")
        .build();
  }

  public final static DomainError employeeNotExist(int id) {
    return ImmutableDomainError.builder()
        .code("7")
        .message("Employee with Id " + id + " does not exist")
        .build();
  }
}

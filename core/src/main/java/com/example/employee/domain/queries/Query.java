package com.example.employee.domain.queries;

import com.example.employee.domain.Environment;
import com.example.employee.domain.entities.DomainError;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;

public interface Query<T> {

  Future<Either<List<DomainError>, T>> response(Environment env);
}

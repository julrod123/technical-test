package com.example.employee.domain.entities;

import io.vavr.collection.List;
import io.vavr.control.Validation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmployeeTest {

    @Test
    public void cannotInstantiatedWithEmptyStrings() {
        final var employee = Employee.from(0, "", "HourlySalaryEmployee", 1, "role", "desc", 20, 9);
        assertEquals(Validation.invalid(List.of(EmployeeErrorFactory.idIsEmpty(), EmployeeErrorFactory.nameIsEmpty())), employee);
    }

    @Test
    public void canBeInstantiatedSuccessfully() {
        final var employee = Employee.from(1, "name", "HourlySalaryEmployee", 1, "role", "desc", 20, 9);
        final var expectedContract = ImmutableContract.builder().type("HourlySalaryEmployee").roleId(1).roleName("role").roleDescription("desc").salary(20).build();
        final var expectedEmployee = ImmutableEmployee.builder().id(1).name("name").contract(expectedContract).build();
        assertEquals(Validation.valid(expectedEmployee), employee);
    }
}

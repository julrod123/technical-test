package com.example.employee.domain.entities;

import io.vavr.control.Validation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContractTest {

  @Test
  public void notValidContract() {
    final var contract = Contract.from("contract", 1, "role", "desc", 1, 1);
    assertEquals(Validation.invalid(ContractErrorFactory.incorrectContract()), contract);
  }

  @Test
  public void cannotBeInstantiatedIdWithZero() {
    final var contract = Contract.from("HourlySalaryEmployee", 0, "role", "desc", 1, 1);
    assertEquals(Validation.invalid(ContractErrorFactory.roleIdIsEmpty()), contract);
  }

  @Test
  public void cannotBeInstantiatedRoleNameWithEmptyString() {
    final var contract = Contract.from("HourlySalaryEmployee", 1, "", "desc", 1, 1);
    assertEquals(Validation.invalid(ContractErrorFactory.roleNameIsEmpty()), contract);
  }

  @Test
  public void cannotBeInstantiatedAmountsWithZero() {
    final var contract = Contract.from("HourlySalaryEmployee", 1, "role", "desc", 0, 0);
    assertEquals(Validation.invalid(ContractErrorFactory.salaryLessOrEqualThanZero()), contract);
  }

  @Test
  public void monthlyContractCanBeInstantiatedSuccesfully() {
    final var contract = Contract.from("MonthlySalaryEmployee", 1, "role", "desc", 20, 9);
    assertEquals(Validation.valid(ImmutableContract.builder().type("MonthlySalaryEmployee").roleId(1).roleName("role").roleDescription("desc").salary(20).build()), contract);
  }

  @Test
  public void hourlyContractCanBeInstantiatedSuccesfully() {
    final var contract = Contract.from("HourlySalaryEmployee", 1, "role", "desc", 20, 9);
    assertEquals(Validation.valid(ImmutableContract.builder().type("HourlySalaryEmployee").roleId(1).roleName("role").roleDescription("desc").salary(9).build()), contract);
  }

  @Test
  public void calculateYearlySalaryForHourlySalaryContract() {
    final var contract = Contract.from("HourlySalaryEmployee", 1, "role", "desc", 20, 9);
    assertEquals(12960.0, contract.get().calculateYearlySalary(), 0.01);
  }

  @Test
  public void calculateYearlySalaryForMonthlySalaryContract() {
    final var contract = Contract.from("MonthlySalaryEmployee", 1, "role", "desc", 20, 9);
    assertEquals(240.0, contract.get().calculateYearlySalary(), 0.01);
  }
}

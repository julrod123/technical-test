plugins {
    java
    id("net.ltgt.apt-idea") version "0.21"
    jacoco
    id("com.diffplug.gradle.spotless") version "3.23.0"
}

allprojects {
    apply(plugin = "com.diffplug.gradle.spotless")
    
    repositories {
        jcenter()
        mavenCentral()
    }

    group = "com.example"
    version = "0.0.1"

    spotless {
        java {
            eclipse().configFile(rootProject.file("formatter-settings.xml"))
        }
    }

    // Needed to run the formatting before the check
    // Spotless by default will fail the build if the code is not formatted
    // This is to format the code before that check everytime the build task runs
    afterEvaluate {
        tasks.getByName("spotlessCheck") {
            dependsOn(tasks.getByName("spotlessApply"))
        }
    }

    ext {
        set("junitVersion", "4.12")
        set("jacocoToolVersion", "0.8.2")
    }
}


jacoco {
    toolVersion = "0.8.2"
}
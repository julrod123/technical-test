# Mas Global Technical test

Project related to Mas Global technical test, information attached inside email from company.

## Dependency

This project uses the dependencies list below:

- [Vavr](https://www.vavr.io/) - Extends Java language with functional properties as scala.
- [Immutables](https://immutables.github.io/) - Extends Java instantiating immutable objects in easy way.
- [Akka](https://akka.io/try-akka/) - Toolbox that give the ability to implement what you need. Specifically in this project, this project uses http and streams. 

## Design

The architecture that I use in this project is base on Ports and Adapters and CQRS. Inside you will find two main modules:
- core: It has all the elements that are relevant for each port and the most important part of the project: Domain.
- read-model-http: This is our query model and it will read all the information stored inside our application, 
also It calls to MasGlobalClient, that is in charge of get all the elements from MasGlobal API endpoint.

## Usage

You only need to run one command 

```sh
$ gradle -q :read-model-http:run
```

This command will compile and run the server. You can interact with postman or via browser with this endpoint
```shell script
http://localhost:8080/employee/<employeeId>
```

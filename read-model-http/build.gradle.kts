plugins {
    application
    id("net.ltgt.apt-idea")
    jacoco
    id("com.bmuschko.docker-java-application") version "4.9.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies{

    compile(project(":core"))

    compileOnly("org.immutables:value:2.7.4")
    annotationProcessor("org.immutables:value:2.7.4")

    compile(group = "io.vavr", name = "vavr-jackson", version = "0.10.2")
    compile(group = "com.fasterxml.jackson.datatype", name = "jackson-datatype-guava", version = "2.9.9")

    compile(group = "com.fasterxml.jackson.module", name = "jackson-module-parameter-names", version = "2.9.9")
    compile(group = "com.fasterxml.jackson.datatype", name = "jackson-datatype-jdk8", version = "2.9.9")
    compile(group = "com.fasterxml.jackson.datatype", name = "jackson-datatype-jsr310", version = "2.9.9")

    //Akka
    compile(group = "com.fasterxml.jackson.core", name = "jackson-databind", version = "2.9.9")
    compile(group = "com.fasterxml.jackson.module", name = "jackson-modules-java8", version = "2.9.9")
    compile(group = "com.typesafe.akka", name = "akka-http_2.12", version = "10.1.8")
    compile(group = "com.typesafe.akka", name = "akka-stream_2.12", version = "2.5.19")
    compile(group = "com.typesafe.akka", name = "akka-http-jackson_2.12", version = "10.1.8")

    compile(group = "com.typesafe", name = "config", version = "1.3.4")

    compile(group = "org.apache.logging.log4j", name = "log4j-api", version = "2.11.2")
    compile(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.11.2")

    //Akka test kit
    testCompile(group = "com.typesafe.akka", name = "akka-http-testkit_2.12", version = "10.1.8")
    testCompile(group = "com.typesafe.akka", name = "akka-stream-testkit_2.12", version = "2.5.19")

    testCompile("junit", "junit", "4.12")

    compileOnly(group ="org.jetbrains",name ="annotations", version = "17.0.0")
}

application {
    mainClassName = "com.example.Main"
    applicationName = "read-model-http"
}

jacoco {
    toolVersion = "0.8.2"
}

docker {
    javaApplication {
        baseImage.set("adoptopenjdk/openjdk11")
        ports.set(listOf(8080))
    }
}
package com.example;

import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.server.Route;
import akka.http.scaladsl.server.util.Tupler;
import akka.stream.ActorMaterializer;
import com.example.employee.domain.Environment;
import com.example.employee.domain.services.EmployeeService;
import com.example.employee.domain.services.impl.EmployeeServiceImpl;
import com.example.employee.infrastructure.cache.EmployeeCache;
import com.example.infrastructure.clients.MasGlobalClient;
import com.example.infrastructure.clients.impls.MasGlobalClientImpl;
import com.example.infrastructure.configuration.AppConfiguration;
import com.example.infrastructure.configuration.HttpConfig;
import com.example.infrastructure.ports.EmployeeRoutes;
import io.vavr.control.Try;
import io.vavr.Tuple;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class Main {

  public static void main(String[] args) {
    final var logger = Logger.getLogger(Main.class.getName());
    AppConfiguration.load()
        .flatMapTry(config -> {
          final ActorSystem system = ActorSystem.create("read-model-http");
          final ActorMaterializer materializer = ActorMaterializer.create(system);
          final ExecutorService executor = Executors.newCachedThreadPool();

          final MasGlobalClient client = new MasGlobalClientImpl(
              system, executor, materializer, config.masGlobalClientConfig(), logger);

          return Try.of(() -> client.get().await().getOrElseThrow(() -> {
            throw new RuntimeException("Error getting employees from MasGlobal");
          })).map(employees -> Tuple.of(config, system, materializer, executor, employees));
        })
        .map(
            config -> {

              final HttpConfig httpConfig = config._1.httpConfig();

              final Environment environment = new Environment(new EmployeeCache(config._4, config._5));
              final EmployeeService employeeService = new EmployeeServiceImpl(environment);

              final Route routes = new EmployeeRoutes(employeeService).routes();
              final var flow = routes.flow(config._2, config._3);

              final var binding = Http.get(config._2)
                  .bindAndHandle(
                      flow,
                      ConnectHttp.toHost(httpConfig.host, httpConfig.port),
                      config._3);

              logger.info(
                  "Server online at http://" + httpConfig.host + ":" + httpConfig.port + "/");

              Runtime.getRuntime()
                  .addShutdownHook(
                      new Thread(
                          () -> Try.of(
                              () -> {
                                logger.info("The System is being turned off");
                                return binding
                                    .thenCompose(ServerBinding::unbind)
                                    .thenAccept(unbound -> config._2.terminate())
                                    .toCompletableFuture()
                                    .get();
                              })));

              return binding;
            })
        .onFailure(
            ex -> {
              logger.severe(ex.getMessage());
              System.exit(255);
            });
  }
}

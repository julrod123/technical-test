package com.example.infrastructure.clients;

import com.example.employee.domain.entities.Employee;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;

public interface MasGlobalClient {

  Future<List<Employee>> get();
}

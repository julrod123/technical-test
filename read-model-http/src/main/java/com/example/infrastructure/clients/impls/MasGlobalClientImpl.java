package com.example.infrastructure.clients.impls;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.Materializer;
import com.example.employee.domain.entities.DomainError;
import com.example.employee.domain.entities.Employee;
import com.example.infrastructure.adapters.dtos.MasGlobalEmployeeDTO;
import com.example.infrastructure.clients.MasGlobalClient;
import com.example.infrastructure.configuration.MasGlobalClientConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Validation;
import io.vavr.jackson.datatype.VavrModule;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public class MasGlobalClientImpl implements MasGlobalClient {

  private final ActorSystem system;
  private final Executor executor;
  private final Materializer materializer;
  private final MasGlobalClientConfig config;
  private final Logger logger;

  public MasGlobalClientImpl(
      ActorSystem system,
      Executor executor,
      Materializer materializer,
      MasGlobalClientConfig config,
      Logger logger) {
    this.system = system;
    this.executor = executor;
    this.materializer = materializer;
    this.config = config;
    this.logger = logger;
  }

  private List<Employee> transformToEmployeeEntity(List<MasGlobalEmployeeDTO> list) {
    return list.flatMap(
        dto -> {
          Validation<List<DomainError>, Employee> employeeValidated = Employee.from(
              dto.getId(),
              dto.getName(),
              dto.getContractTypeName(),
              dto.getRoleId(),
              dto.getRoleName(),
              dto.getRoleDescription(),
              dto.getHourlySalary(),
              dto.getMonthlySalary());

          return employeeValidated.fold(
              domainErrors -> {
                domainErrors.forEach(error -> logger.warning("Error: code " + error.code() + " message: " + error.message()));
                return List.empty();
              },
              List::of);
        });
  }

  @Override
  public Future<List<Employee>> get() {

    final CompletableFuture<HttpResponse> responseFuture = Http.get(system).singleRequest(HttpRequest.create(config.url)).toCompletableFuture();

    Future<List<Employee>> response = Future.fromCompletableFuture(executor, responseFuture)
        .flatMap(
            httpResponse -> {
              final ObjectMapper mapper = new ObjectMapper();
              mapper.registerModule(new Jdk8Module());

              CompletionStage<MasGlobalEmployeeDTO[]> entity = Jackson.unmarshaller(mapper, MasGlobalEmployeeDTO[].class)
                  .unmarshal(httpResponse.entity(), materializer);

              MasGlobalEmployeeDTO[] rl = entity.toCompletableFuture().join();

              return Future.fromCompletableFuture(executor, entity.toCompletableFuture())
                  .map(r -> transformToEmployeeEntity(List.ofAll(Arrays.asList(r))));
            });

    return response;
  }
}

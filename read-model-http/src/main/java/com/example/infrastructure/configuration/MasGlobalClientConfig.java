package com.example.infrastructure.configuration;

import com.typesafe.config.Config;

public class MasGlobalClientConfig {

  public final String url;

  private MasGlobalClientConfig(String url) {
    this.url = url;
  }

  static final MasGlobalClientConfig from(Config config) {
    return new MasGlobalClientConfig(config.getString("mas-global-client.url"));
  }
}

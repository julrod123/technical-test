package com.example.infrastructure.configuration;

import com.typesafe.config.Config;

public class HttpConfig {

  public final String host;
  public final int port;

  private HttpConfig(String host, int port) {
    this.host = host;
    this.port = port;
  }

  static final HttpConfig from(Config config) {
    return new HttpConfig(
        config.getString("http.host"),
        config.getInt("http.port"));
  }

}

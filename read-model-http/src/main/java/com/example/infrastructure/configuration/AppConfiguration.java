package com.example.infrastructure.configuration;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.vavr.control.Try;

public abstract class AppConfiguration {

  private AppConfiguration() {}

  public abstract HttpConfig httpConfig();

  public abstract MasGlobalClientConfig masGlobalClientConfig();

  public static final Try<AppConfiguration> load() {
    return Try.of(
        () -> {
          Config config = ConfigFactory.load();
          return new AppConfiguration() {
            final HttpConfig httpConfig = HttpConfig.from(config);
            final MasGlobalClientConfig masGlobalClientConfig = MasGlobalClientConfig.from(config);

            @Override
            public HttpConfig httpConfig() {
              return httpConfig;
            }

            @Override
            public MasGlobalClientConfig masGlobalClientConfig() {
              return masGlobalClientConfig;
            }
          };
        });
  }
}

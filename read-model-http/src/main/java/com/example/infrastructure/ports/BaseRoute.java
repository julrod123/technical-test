package com.example.infrastructure.ports;

import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import com.example.employee.domain.entities.DomainError;
import com.example.infrastructure.adapters.dtos.ServiceErrorDTO;
import io.vavr.Function1;
import io.vavr.collection.List;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;

import java.util.logging.Logger;

import static akka.http.javadsl.marshallers.jackson.Jackson.marshaller;

public abstract class BaseRoute extends AllDirectives {

  private final Logger logger = Logger.getLogger(getClass().getName());

  final protected <A> Route completeFuture(
      Future<Either<List<DomainError>, A>> either,
      Function1<List<DomainError>, Route> onServiceError,
      Function1<A, Route> onSuccess) {
    return onComplete(either.toCompletableFuture(), t -> t.fold(ex -> {
      logger.warning(ex.getMessage());
      return complete(
          StatusCodes.INTERNAL_SERVER_ERROR,
          ServiceErrorDTO.from(
              "500",
              "The request couldn't be processed, try again later"),
          marshaller());
    }, e -> e.fold(onServiceError, onSuccess)));
  }
}

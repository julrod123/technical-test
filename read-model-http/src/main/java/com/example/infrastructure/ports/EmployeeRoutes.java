package com.example.infrastructure.ports;

import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.Route;
import com.example.employee.domain.services.EmployeeService;
import com.example.infrastructure.adapters.dtos.EmployeeResponseDTO;
import com.example.infrastructure.adapters.dtos.ServiceErrorDTO;

import java.util.Objects;

import static akka.http.javadsl.marshallers.jackson.Jackson.marshaller;

public class EmployeeRoutes extends BaseRoute {

  private EmployeeService employeeService;

  public EmployeeRoutes(EmployeeService employeeService) {
    Objects.requireNonNull(getEmployeeById, "User Service cannot be null, please provide one");
    this.employeeService = employeeService;
  }

  public Route routes() {
    return concat(getEmployeeById);
  }

  private Route getEmployeeById = pathPrefix(
      "employee",
      () -> get(
          () -> path(
              s -> completeFuture(
                  employeeService.getEmployeeById(Integer.parseInt(s)),
                  errors -> complete(
                      StatusCodes.BAD_REQUEST,
                      errors.map(ServiceErrorDTO::from).toJavaList(),
                      marshaller()),
                  result -> complete(
                      StatusCodes.OK,
                      new EmployeeResponseDTO(result),
                      marshaller())))));
}

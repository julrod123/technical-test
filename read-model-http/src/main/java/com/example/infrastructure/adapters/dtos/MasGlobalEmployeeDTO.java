package com.example.infrastructure.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MasGlobalEmployeeDTO {

  int id;
  String name;
  String contractTypeName;
  int roleId;
  String roleName;
  String roleDescription;
  double hourlySalary;
  double monthlySalary;

  public MasGlobalEmployeeDTO() {}

  public MasGlobalEmployeeDTO(int id, String name, String contractTypeName, int roleId, String roleName, String roleDescription, double hourlySalary, double monthlySalary) {
    this.id = id;
    this.name = name;
    this.contractTypeName = contractTypeName;
    this.roleId = roleId;
    this.roleName = roleName;
    this.roleDescription = roleDescription;
    this.hourlySalary = hourlySalary;
    this.monthlySalary = monthlySalary;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getContractTypeName() {
    return contractTypeName;
  }

  public int getRoleId() {
    return roleId;
  }

  public String getRoleName() {
    return roleName;
  }

  public String getRoleDescription() {
    return this.roleDescription != null ? this.roleDescription : "";
  }

  public double getHourlySalary() {
    return hourlySalary;
  }

  public double getMonthlySalary() {
    return monthlySalary;
  }
}

package com.example.infrastructure.adapters.dtos;

import com.example.employee.domain.entities.Contract;
import com.example.employee.domain.entities.Employee;

class ContractResponseDTO {

  public final String contractType;
  public final int roleId;
  public final String roleName;
  public final String roleDescription;
  public final double salary;
  public final double annualSalary;

  public ContractResponseDTO(Contract contract) {
    this.contractType = contract.type();
    this.roleId = contract.roleId();
    this.roleName = contract.roleName();
    this.roleDescription = contract.roleDescription();
    this.salary = contract.salary();
    this.annualSalary = contract.calculateYearlySalary();
  }
}

public class EmployeeResponseDTO {

  public final int id;
  public final String name;
  public final ContractResponseDTO contract;

  public EmployeeResponseDTO(Employee employee) {
    this.id = employee.id();
    this.name = employee.name();
    this.contract = new ContractResponseDTO(employee.contract());
  }
}

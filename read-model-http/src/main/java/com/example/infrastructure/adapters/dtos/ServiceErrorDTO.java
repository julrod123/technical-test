package com.example.infrastructure.adapters.dtos;

import com.example.employee.domain.entities.DomainError;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(builder = "new") // builder has to have constructor
@JsonDeserialize(builder = ImmutableServiceErrorDTO.Builder.class)
public abstract class ServiceErrorDTO {

  public abstract String code();

  public abstract String message();

  public static ServiceErrorDTO from(DomainError error) {
    return from(error.code(), error.message());
  }

  public static ServiceErrorDTO from(String code, String message) {
    return new ImmutableServiceErrorDTO.Builder().code(code).message(message).build();
  }
}
